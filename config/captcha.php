<?php
/*
 * Secret key and Site key get on https://www.google.com/recaptcha
 * */
return [
  'secret' => env('CAPTCHA_SECRET', '6Ld8qHoUAAAAAM732wrH8GvUDud3wDsp6GrSe6o-'),
  'sitekey' => env('CAPTCHA_SITEKEY', '6Ld8qHoUAAAAAC7L1VboG7Hje1_ia43GDf-dTKMr'),
    /**
     * @var string|null Default ``null``.
     * Custom with function name (example customRequestCaptcha) or class@method (example \App\CustomRequestCaptcha@custom).
     * Function must be return instance, read more in folder ``examples``
     */
    'request_method' => null,
    'options' => [
        'multiple' => false,
        'lang' => app()->getLocale(),
    ],
    'options' => [

        'multiple' => false,

        // 'lang' => app()->getLocale(),

        'lang' => 'tw',

    ],

    'attributes' => [
        'theme' => 'light'
    ],
];
