-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2019 年 04 月 23 日 04:36
-- 伺服器版本: 10.1.36-MariaDB
-- PHP 版本： 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `express`
--

-- --------------------------------------------------------

--
-- 資料表結構 `admin_log`
--

CREATE TABLE `admin_log` (
  `id` int(11) NOT NULL,
  `sid` int(11) DEFAULT NULL,
  `ip` varchar(200) NOT NULL,
  `updateDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `admin_log`
--

INSERT INTO `admin_log` (`id`, `sid`, `ip`, `updateDate`) VALUES
(21, 1, '127.0.0.1', '2018-11-07 18:07:17'),
(20, 1, '127.0.0.1', '2018-11-07 14:45:05'),
(19, 1, '127.0.0.1', '2018-11-07 10:24:30'),
(18, 1, '127.0.0.1', '2018-11-07 09:32:53'),
(17, 1, '127.0.0.1', '2018-11-05 14:03:15'),
(16, 1, '127.0.0.1', '2018-11-05 11:13:20'),
(15, 1, '127.0.0.1', '2018-11-04 20:53:14'),
(14, 1, '127.0.0.1', '2018-11-04 17:55:26'),
(22, 1, '::1', '2018-11-20 09:35:59'),
(23, 1, '::1', '2018-11-20 13:40:43'),
(24, 1, '::1', '2018-11-20 14:59:44'),
(25, 1, '::1', '2018-11-21 10:35:17'),
(26, 1, '::1', '2018-11-21 16:01:09'),
(27, 1, '::1', '2018-11-21 17:03:31'),
(28, 1, '::1', '2018-12-10 13:14:38'),
(29, 1, '::1', '2018-12-26 14:14:07'),
(30, 1, '::1', '2019-01-02 14:58:18'),
(31, 1, '::1', '2019-01-21 11:33:13'),
(32, 1, '127.0.0.1', '2019-02-27 02:20:47'),
(33, 1, '127.0.0.1', '2019-02-27 02:20:51'),
(34, 1, '127.0.0.1', '2019-02-27 02:21:30'),
(35, 1, '127.0.0.1', '2019-02-27 02:32:43'),
(36, 1, '127.0.0.1', '2019-02-27 03:00:10'),
(37, 1, '127.0.0.1', '2019-02-27 03:00:27'),
(38, 1, '127.0.0.1', '2019-02-27 03:20:17'),
(39, 1, '127.0.0.1', '2019-02-27 03:20:31'),
(40, 1, '127.0.0.1', '2019-02-27 03:22:24'),
(41, 1, '127.0.0.1', '2019-02-27 03:32:25'),
(42, 1, '127.0.0.1', '2019-02-27 03:32:59'),
(43, 27, '127.0.0.1', '2019-02-27 03:33:20'),
(44, 1, '127.0.0.1', '2019-02-27 03:33:30'),
(45, 1, '127.0.0.1', '2019-02-27 10:13:51'),
(46, 1, '127.0.0.1', '2019-03-04 01:12:53'),
(47, 1, '127.0.0.1', '2019-03-04 09:10:13'),
(48, 1, '127.0.0.1', '2019-03-05 01:16:55'),
(49, 1, '127.0.0.1', '2019-03-06 01:16:31'),
(50, 1, '127.0.0.1', '2019-03-07 09:13:40'),
(51, 27, '127.0.0.1', '2019-03-07 09:17:20'),
(52, 27, '127.0.0.1', '2019-03-07 09:17:32'),
(53, 1, '127.0.0.1', '2019-03-08 09:26:05'),
(54, 1, '127.0.0.1', '2019-03-08 09:26:29'),
(55, 1, '127.0.0.1', '2019-03-08 09:27:00'),
(56, 1, '127.0.0.1', '2019-03-11 09:14:34'),
(57, 1, '127.0.0.1', '2019-03-11 14:42:49'),
(58, 1, '127.0.0.1', '2019-03-18 09:53:03'),
(59, 1, '127.0.0.1', '2019-03-19 17:27:11'),
(60, 1, '127.0.0.1', '2019-03-20 17:19:09'),
(61, 1, '127.0.0.1', '2019-03-21 18:33:34'),
(62, 1, '127.0.0.1', '2019-04-11 15:49:02');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL,
  `mcode` varchar(20) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `realname` varchar(120) NOT NULL,
  `pic` varchar(200) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `func1` char(1) DEFAULT 'Y',
  `func2` char(1) DEFAULT 'Y',
  `func3` char(1) DEFAULT 'Y',
  `func4` char(1) DEFAULT 'Y',
  `func5` char(1) DEFAULT 'Y',
  `func6` char(1) DEFAULT 'Y',
  `func7` char(1) DEFAULT 'N',
  `enable` char(1) NOT NULL DEFAULT 'Y',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `admin_user`
--

INSERT INTO `admin_user` (`id`, `mcode`, `level`, `parent`, `username`, `password`, `realname`, `pic`, `logo`, `phone`, `func1`, `func2`, `func3`, `func4`, `func5`, `func6`, `func7`, `enable`, `createDate`, `updateDate`) VALUES
(1, NULL, 1, NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', '後台總管理員', '5bd942e949e8e.png', NULL, NULL, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, NULL, 2, 1, 'test', '098f6bcd4621d373cade4e832627b4f6', 'test', '5be2b51b26999.png', NULL, NULL, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'N', 'Y', '2018-11-07 17:49:15', '0000-00-00 00:00:00'),
(43, 'm00004', 3, 0, 'eason2', '50bc2239961ae0b3941564f3a968463a', 'eason2', '5be2b51b26999.png', NULL, '0928888888', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-04-12 03:36:42', '2019-04-12 03:36:42'),
(42, 'm00003', 3, 0, 'eason3', '855d1e1e5b8f7b639a8af6dab8e4249c', 'eason3', '5be2b51b26999.png', NULL, '0928888888', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-04-12 03:36:31', '2019-04-12 03:36:31'),
(39, 'm00001', 3, 0, 'eason', '75eb724c956efca35b882fa71b099455', 'eason', '5be2b51b26999.png', NULL, '0928888888', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', '2019-04-12 03:35:54', '2019-04-12 03:35:54');

-- --------------------------------------------------------

--
-- 資料表結構 `col`
--

CREATE TABLE `col` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store` int(10) DEFAULT '0',
  `type` int(11) DEFAULT NULL COMMENT '1 物流 2 門市 3 狀態',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `col`
--

INSERT INTO `col` (`id`, `name`, `name2`, `store`, `type`, `createDate`, `updateDate`) VALUES
(1, 'UNIMARTC2C', '7-11	', 0, 1, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(2, 'FAMIC2C', '全家', 0, 1, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(3, 'HILIFEC2C', '萊爾富', 0, 1, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(4, '011629', '全家基隆廟口店', 2, 2, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(5, '896218', '萊爾富竹北起飛店', 3, 2, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(6, '149697', '7-11三興門市', 1, 2, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(7, '3750', '萊爾富北市湖鑽店', 3, 2, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(8, '00005', '現場退貨', 0, 3, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(9, '00002', '超商退貨', 0, 3, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(10, '00003', '退大陸', 0, 3, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(11, '00004', '非我司', 0, 3, '2019-04-15 00:00:00', '2019-04-15 00:00:00'),
(12, '00001', '寄貨', 0, 3, '2019-04-16 00:00:00', '2019-04-16 00:00:00'),
(13, '014482', '全家通昌店', 2, 2, '2019-04-18 00:00:00', '2019-04-18 00:00:00'),
(14, '189075', '永富門市', 1, 2, '2019-04-18 00:00:00', '2019-04-18 00:00:00'),
(15, '015680', '全家新竹巨城店', 2, 2, '2019-04-18 00:00:00', '2019-04-18 00:00:00'),
(16, '001136', '全家新竹旺宏店', 2, 2, '2019-04-18 00:00:00', '2019-04-18 00:00:00'),
(17, '011147', '全家新竹旺宏三店', 2, 2, '2019-04-22 00:00:00', '2019-04-22 00:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `lgstatus`
--

CREATE TABLE `lgstatus` (
  `id` int(11) NOT NULL,
  `lgtype` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lgcompany` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lgcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lgmsg` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `lgstatus`
--

INSERT INTO `lgstatus` (`id`, `lgtype`, `lgcompany`, `lgcode`, `lgmsg`) VALUES
(1, 'CVS', 'FAMI', '300', '訂單處理中(已收到訂單資料)'),
(2, 'CVS', 'FAMI', '7007', '門市遺失'),
(3, 'CVS', 'FAMI', '7006', '小物流遺失'),
(4, 'CVS', 'FAMI', '7008', '小物流破損，退回物流中心'),
(5, 'CVS', 'FAMI', '310', '上傳電子訂單檔處理中'),
(6, 'CVS', 'FAMI', '7009', '商品包裝不良（物流中心反應）'),
(7, 'CVS', 'FAMI', '7010', '商品包裝不良（門市反應）'),
(8, 'CVS', 'FAMI', '7011', '取件門市閉店，轉退回原寄件店'),
(9, 'CVS', 'FAMI', '7012', '條碼錯誤，物流中心客服處理'),
(10, 'CVS', 'FAMI', '7015', '條碼重複，物流中心客服處理'),
(11, 'CVS', 'FAMI', '5009', '進貨門市發生緊急閉店，提早退貨至物流中心'),
(12, 'CVS', 'FAMI', '7016', '超才'),
(13, 'CVS', 'FAMI', '7013', '訂單超過驗收期限（商家未出貨）'),
(14, 'CVS', 'FAMI', '7014', '商家未到貨（若訂單成立隔日未到貨即會發送，直到訂單失效刪除）'),
(15, 'CVS', 'FAMI', '4001', '退貨商品已至門市交寄'),
(16, 'CVS', 'FAMI', '4002', '退貨商品已至物流中心'),
(17, 'CVS', 'FAMI', '3024', '貨件已至物流中心'),
(18, 'CVS', 'FAMI', '3025', '退貨已退回物流中心'),
(19, 'CVS', 'FAMI', '3020', '貨件未取退回物流中心'),
(20, 'CVS', 'FAMI', '3021', '退貨商品未取退回物流中心'),
(21, 'CVS', 'FAMI', '3032', '賣家已到門市寄件'),
(22, 'CVS', 'FAMI', '3022', '買家已到店取貨'),
(23, 'CVS', 'FAMI', '3023', '賣家已取買家未取貨'),
(24, 'CVS', 'FAMI', '3018', '到店尚未取貨，簡訊通知取件'),
(25, 'CVS', 'FAMI', '3029', '商品已轉換店（商品送達指定更換之取件店舖）'),
(26, 'CVS', 'FAMI', '3019', '退件到店尚未取貨，簡訊通知取件'),
(27, 'CVS', 'FAMI', '3031', '退貨商品已轉換店（退貨商品送達指定更換之取件店舖）'),
(28, 'CVS', 'UNIMART', '300', '訂單處理中(已收到訂單資料)'),
(29, 'CVS', 'UNIMART', '2030', '物流中心驗收成功'),
(30, 'CVS', 'UNIMART', '2044', '物流中心驗收退貨完畢'),
(31, 'CVS', 'UNIMART', '2046', '貨件未取退回大智通物流中心'),
(32, 'CVS', 'UNIMART', '2007', '商品類型為空'),
(33, 'CVS', 'UNIMART', '2008', '訂單為空'),
(34, 'CVS', 'UNIMART', '2009', '門市店號為空'),
(35, 'CVS', 'UNIMART', '2010', '出貨日期為空'),
(36, 'CVS', 'UNIMART', '2011', '出貨金額為空'),
(37, 'CVS', 'UNIMART', '2012', '出貨編號不存在'),
(38, 'CVS', 'UNIMART', '2013', '母廠商不存在'),
(39, 'CVS', 'UNIMART', '2014', '子廠商不存在'),
(40, 'CVS', 'UNIMART', '2015', '出貨編號已存在(單筆)'),
(41, 'CVS', 'UNIMART', '2016', '門市已關轉店，將進行退貨處理'),
(42, 'CVS', 'UNIMART', '2017', '出貨日期不符合規定'),
(43, 'CVS', 'UNIMART', '2018', '服務類型不符規定(如只開取貨付款服務，確使用純取貨服務)'),
(44, 'CVS', 'UNIMART', '2019', '商品類型不符規定'),
(45, 'CVS', 'UNIMART', '2020', '廠商尚未申請店配服務'),
(46, 'CVS', 'UNIMART', '2021', '同一批次出貨編號重覆(批次)'),
(47, 'CVS', 'UNIMART', '2022', '出貨金額不符規定'),
(48, 'CVS', 'UNIMART', '2023', '取貨人姓名為空'),
(49, 'CVS', 'UNIMART', '2057', '車輛故障，後續配送中'),
(50, 'CVS', 'UNIMART', '2058', '天候不佳，後續配送中'),
(51, 'CVS', 'UNIMART', '2059', '道路中斷，後續配送中'),
(52, 'CVS', 'UNIMART', '2060', '門市停業中，將進行退貨處理'),
(53, 'CVS', 'UNIMART', '2061', '缺件(商品未至門市)'),
(54, 'CVS', 'UNIMART', '2062', '門市報缺'),
(55, 'CVS', 'UNIMART', '2047', '正常二退(退貨時間延長，在判賠期限內退回)'),
(56, 'CVS', 'UNIMART', '2024', '物流作業驗收中'),
(57, 'CVS', 'UNIMART', '2025', '門市轉店號(舊門市店號已更新)'),
(58, 'CVS', 'UNIMART', '2026', '無此門市，將進行退貨處理'),
(59, 'CVS', 'UNIMART', '2027', '門市指定時間不配送(六、日)'),
(60, 'CVS', 'UNIMART', '2028', '門市關轉店，3日內未更新SUP(新店號)便至退貨流程'),
(61, 'CVS', 'UNIMART', '2029', '門市尚未開店'),
(62, 'CVS', 'UNIMART', '2031', '未到貨(物流端未收到該商品)'),
(63, 'CVS', 'UNIMART', '2063', '門市配達'),
(64, 'CVS', 'UNIMART', '2048', '商品瑕疵(商品在物流中心)'),
(65, 'CVS', 'UNIMART', '2049', '門市關店，將進行退貨處理'),
(66, 'CVS', 'UNIMART', '2050', '門市轉店，將進行退貨處理'),
(67, 'CVS', 'UNIMART', '2051', '廠商要求提早退貨（廠商出錯商品）'),
(68, 'CVS', 'UNIMART', '2052', '違禁品(退貨及罰款處理)'),
(69, 'CVS', 'UNIMART', '2095', '天候路況不佳'),
(70, 'CVS', 'UNIMART', '2065', 'EC收退'),
(71, 'CVS', 'UNIMART', '2066', '異常收退(商品破損、外袋破損、消費者取錯件、誤刷代收等，提早從門市退貨)'),
(72, 'CVS', 'UNIMART', '2053', '刷A給B'),
(73, 'CVS', 'UNIMART', '2054', '消費者要求提早拉退（消費者下訂單後又跟廠商取消）'),
(74, 'CVS', 'UNIMART', '2032', '商品瑕疵(進物流中心)'),
(75, 'CVS', 'UNIMART', '2033', '超材'),
(76, 'CVS', 'UNIMART', '2034', '違禁品(退貨及罰款處理)'),
(77, 'CVS', 'UNIMART', '2035', '訂單資料重覆上傳'),
(78, 'CVS', 'UNIMART', '2036', '已過門市進貨日（未於指定時間內寄至物流中心）'),
(79, 'CVS', 'UNIMART', '2038', '第一段標籤規格錯誤'),
(80, 'CVS', 'UNIMART', '2039', '第一段標籤無法判讀'),
(81, 'CVS', 'UNIMART', '2040', '第一段標籤資料錯誤'),
(82, 'CVS', 'UNIMART', '2041', '物流中心理貨中'),
(83, 'CVS', 'UNIMART', '2042', '商品遺失'),
(84, 'CVS', 'UNIMART', '2043', '門市指定不配送(六、日)'),
(85, 'CVS', 'UNIMART', '2045', '不正常到貨(商品提早到物流中心)'),
(86, 'CVS', 'UNIMART', '2002', '出貨單號不合規則'),
(87, 'CVS', 'UNIMART', '2003', 'XML檔內出貨單號重複'),
(88, 'CVS', 'UNIMART', '2004', '出貨單號重複上傳使用(驗收時發現)'),
(89, 'CVS', 'UNIMART', '2005', '日期格式不符'),
(90, 'CVS', 'UNIMART', '2006', '訂單金額或代收金額錯誤'),
(91, 'CVS', 'UNIMART', '2071', '門市代碼格式錯誤'),
(92, 'CVS', 'UNIMART', '2073', '商品配達買家取貨門市'),
(93, 'CVS', 'UNIMART', '2074', '消費者七天未取，商品離開買家取貨門市'),
(94, 'CVS', 'UNIMART', '2072', '商品配達賣家取退貨門市'),
(95, 'CVS', 'UNIMART', '2075', '廠商未至門市取退貨，商品離開賣家取退貨門市'),
(96, 'CVS', 'UNIMART', '2001', '檔案傳送成功'),
(97, 'CVS', 'UNIMART', '310', '上傳電子訂單檔處理中'),
(98, 'CVS', 'UNIMART', '4002', '退貨商品已至物流中心'),
(99, 'CVS', 'UNIMART', '2094', '包裹異常不配送'),
(100, 'CVS', 'UNIMART', '2037', '門市關轉(可使用SUP檔案更新原單號更新門市與出貨日)'),
(101, 'CVS', 'UNIMART', '2070', '退回原寄件門市且已取件'),
(102, 'CVS', 'UNIMART', '2067', '消費者成功取件'),
(103, 'CVS', 'UNIMART', '7021', '商品捆包'),
(104, 'CVS', 'UNIMART', '7022', '商品外袋透明'),
(105, 'CVS', 'UNIMART', '7023', '多標籤'),
(106, 'CVS', 'UNIMART', '2055', '更換門市'),
(107, 'CVS', 'UNIMART', '7017', '取件包裹異常協尋中'),
(108, 'CVS', 'UNIMART', '7018', '取件遺失進行賠償程序'),
(109, 'CVS', 'UNIMART', '4001', '退貨商品已至門市交寄'),
(110, 'CVS', 'UNIMARTC2C', '2030', '物流中心驗收成功'),
(111, 'CVS', 'UNIMARTC2C', '7019', '寄件貨態異常協尋中'),
(112, 'CVS', 'UNIMARTC2C', '2068', '交貨便收件(A門市收到件寄件商品)'),
(113, 'CVS', 'UNIMARTC2C', '2069', '退貨便收件(商品退回指定C門市)'),
(114, 'CVS', 'UNIMARTC2C', '7020', '寄件遺失進行賠償程序'),
(115, 'CVS', 'UNIMARTC2C', '2073', '商品配達買家取貨門市'),
(116, 'CVS', 'UNIMARTC2C', '2074', '消費者七天未取，商品離開買家取貨門市'),
(117, 'CVS', 'UNIMARTC2C', '2101', '門市關轉店'),
(118, 'CVS', 'UNIMARTC2C', '2102', '門市舊店號更新'),
(119, 'CVS', 'UNIMARTC2C', '2103', '無取件門市資料'),
(120, 'CVS', 'UNIMARTC2C', '2104', '門市臨時關轉店'),
(121, 'CVS', 'UNIMARTC2C', '2076', '消費者七天未取，商品退回至大智通'),
(122, 'CVS', 'UNIMARTC2C', '2077', '廠商未至門市取退貨，商品退回至大智通'),
(123, 'CVS', 'UNIMARTC2C', '2078', '買家未取貨退回物流中心-驗收成功'),
(124, 'CVS', 'UNIMARTC2C', '2079', '買家未取貨退回物流中心-商品瑕疵(進物流中心)'),
(125, 'CVS', 'UNIMARTC2C', '2080', '買家未取貨退回物流中心-超材'),
(126, 'CVS', 'UNIMARTC2C', '2081', '買家未取貨退回物流中心-違禁品(退貨及罰款處理)'),
(127, 'CVS', 'UNIMARTC2C', '2082', '買家未取貨退回物流中心-訂單資料重覆上傳'),
(128, 'CVS', 'UNIMARTC2C', '2083', '買家未取貨退回物流中心-已過門市進貨日（未於指定時間內寄至物流中心）'),
(129, 'CVS', 'UNIMARTC2C', '2092', '買家未取退回物流中心-門市關轉'),
(130, 'CVS', 'UNIMARTC2C', '2084', '買家未取貨退回物流中心-第一段標籤規格錯誤'),
(131, 'CVS', 'UNIMARTC2C', '2085', '買家未取貨退回物流中心-第一段標籤無法判讀'),
(132, 'CVS', 'UNIMARTC2C', '2086', '買家未取貨退回物流中心-第一段標籤資料錯誤'),
(133, 'CVS', 'UNIMARTC2C', '2087', '買家未取貨退回物流中心-物流中心理貨中'),
(134, 'CVS', 'UNIMARTC2C', '2088', '買家未取貨退回物流中心-商品遺失'),
(135, 'CVS', 'UNIMARTC2C', '2089', '買家未取退回物流中心-門市指定不配送(六、日)'),
(136, 'CVS', 'UNIMARTC2C', '2093', '買家未取退回物流中心-爆量'),
(137, 'CVS', 'UNIMARTC2C', '2067', '消費者成功取件'),
(138, 'CVS', 'HILIFE', '300', '訂單處理中(已收到訂單資料)'),
(139, 'CVS', 'HILIFE', '310', '上傳電子訂單檔處理中'),
(140, 'CVS', 'HILIFE', '311', '上傳退貨電子訂單處理中'),
(141, 'CVS', 'HILIFE', '325', '退貨訂單處理中(已收到訂單資料)'),
(142, 'CVS', 'HILIFE', '2000', '出貨訂單修改'),
(143, 'CVS', 'HILIFE', '2001', '檔案傳送成功'),
(144, 'CVS', 'HILIFE', '2002', '出貨單號不合規則'),
(145, 'CVS', 'HILIFE', '2003', 'XML檔內出貨單號重複'),
(146, 'CVS', 'HILIFE', '2004', '出貨單號重複上傳使用(驗收時發現)'),
(147, 'CVS', 'HILIFE', '2005', '日期格式不符'),
(148, 'CVS', 'HILIFE', '2006', '訂單金額或代收金額錯誤'),
(149, 'CVS', 'HILIFE', '2007', '商品類型為空'),
(150, 'CVS', 'HILIFE', '2009', '門市店號為空'),
(151, 'CVS', 'HILIFE', '2010', '出貨日期為空'),
(152, 'CVS', 'HILIFE', '2011', '出貨金額為空'),
(153, 'CVS', 'HILIFE', '2012', '出貨編號不存在'),
(154, 'CVS', 'HILIFE', '2013', '母廠商不存在'),
(155, 'CVS', 'HILIFE', '2014', '子廠商不存在'),
(156, 'CVS', 'HILIFE', '2015', '出貨編號已存在(單筆)'),
(157, 'CVS', 'HILIFE', '2016', '門市已關轉店，將進行退貨處理'),
(158, 'CVS', 'HILIFE', '2017', '出貨日期不符合規定'),
(159, 'CVS', 'HILIFE', '2018', '服務類型不符規定(如只開取貨付款服務，確使用純取貨服務)'),
(160, 'CVS', 'HILIFE', '2019', '商品類型不符規定'),
(161, 'CVS', 'HILIFE', '2020', '廠商尚未申請店配服務'),
(162, 'CVS', 'HILIFE', '2021', '同一批次出貨編號重覆(批次)'),
(163, 'CVS', 'HILIFE', '2022', '出貨金額不符規定'),
(164, 'CVS', 'HILIFE', '2023', '取貨人姓名為空'),
(165, 'CVS', 'HILIFE', '2024', '物流作業驗收中'),
(166, 'CVS', 'HILIFE', '2025', '門市轉店號(舊門市店號已更新)'),
(167, 'CVS', 'HILIFE', '2026', '無此門市，將進行退貨處理'),
(168, 'CVS', 'HILIFE', '2027', '門市指定時間不配送(六、日)'),
(169, 'CVS', 'HILIFE', '2028', '門市關轉店，3日內未更新SUP(新店號)便至退貨流程'),
(170, 'CVS', 'HILIFE', '2029', '門市尚未開店'),
(171, 'CVS', 'HILIFE', '2030', '物流中心驗收成功'),
(172, 'CVS', 'HILIFE', '2031', '未到貨(物流端未收到該商品)'),
(173, 'CVS', 'HILIFE', '2032', '商品瑕疵(進物流中心)'),
(174, 'CVS', 'HILIFE', '2033', '超材'),
(175, 'CVS', 'HILIFE', '2034', '違禁品(退貨及罰款處理)'),
(176, 'CVS', 'HILIFE', '2035', '訂單資料重覆上傳'),
(177, 'CVS', 'HILIFE', '2036', '已過門市進貨日（未於指定時間內寄至物流中心）'),
(178, 'CVS', 'HILIFE', '2037', '門市關轉(可使用SUP檔案更新原單號更新門市與出貨日)'),
(179, 'CVS', 'HILIFE', '2038', '第一段標籤規格錯誤'),
(180, 'CVS', 'HILIFE', '2039', '第一段標籤無法判讀'),
(181, 'CVS', 'HILIFE', '2040', '第一段標籤資料錯誤'),
(182, 'CVS', 'HILIFE', '2041', '物流中心理貨中'),
(183, 'CVS', 'HILIFE', '2042', '商品遺失'),
(184, 'CVS', 'HILIFE', '2043', '門市指定不配送(六、日)'),
(185, 'CVS', 'HILIFE', '2045', '不正常到貨(商品提早到物流中心)'),
(186, 'CVS', 'HILIFE', '2046', '貨件未取退回大智通物流中心'),
(187, 'CVS', 'HILIFE', '2047', '正常二退(退貨時間延長，在判賠期限內退回)'),
(188, 'CVS', 'HILIFE', '2048', '商品瑕疵(商品在物流中心)'),
(189, 'CVS', 'HILIFE', '2049', '門市關店，將進行退貨處理'),
(190, 'CVS', 'HILIFE', '2050', '門市轉店，將進行退貨處理'),
(191, 'CVS', 'HILIFE', '2051', '廠商要求提早退貨（廠商出錯商品）'),
(192, 'CVS', 'HILIFE', '2052', '違禁品(退貨及罰款處理)'),
(193, 'CVS', 'HILIFE', '2053', '刷A給B'),
(194, 'CVS', 'HILIFE', '2054', '消費者要求提早拉退（消費者下訂單後又跟廠商取消）'),
(195, 'CVS', 'HILIFE', '2055', '更換門市'),
(196, 'CVS', 'HILIFE', '2057', '車輛故障，後續配送中'),
(197, 'CVS', 'HILIFE', '2058', '天候不佳，後續配送中'),
(198, 'CVS', 'HILIFE', '2059', '道路中斷，後續配送中'),
(199, 'CVS', 'HILIFE', '2060', '門市停業中，將進行退貨處理'),
(200, 'CVS', 'HILIFE', '2061', '缺件(商品未至門市)'),
(201, 'CVS', 'HILIFE', '2062', '門市報缺'),
(202, 'CVS', 'HILIFE', '2063', '門市配達'),
(203, 'CVS', 'HILIFE', '2065', 'EC收退'),
(204, 'CVS', 'HILIFE', '2066', '異常收退(商品破損、外袋破損、消費者取錯件、誤刷代收等，提早從門市退貨)'),
(205, 'CVS', 'HILIFE', '2067', '消費者成功取件'),
(206, 'CVS', 'HILIFE', '2068', '交貨便收件(A門市收到件寄件商品)'),
(207, 'CVS', 'HILIFE', '2069', '退貨便收件(商品退回指定C門市)'),
(208, 'CVS', 'HILIFE', '2070', '退回原寄件門市且已取件'),
(209, 'CVS', 'HILIFE', '2071', '門市代碼格式錯誤'),
(210, 'CVS', 'HILIFE', '2072', '商品配達賣家取退貨門市'),
(211, 'CVS', 'HILIFE', '2073', '商品配達買家取貨門市'),
(212, 'CVS', 'HILIFE', '2074', '消費者七天未取，商品離開買家取貨門市'),
(213, 'CVS', 'HILIFE', '2075', '廠商未至門市取退貨，商品離開賣家取退貨門市'),
(214, 'CVS', 'HILIFE', '2101', '門市關轉店'),
(215, 'CVS', 'HILIFE', '2102', '門市舊店號更新'),
(216, 'CVS', 'HILIFE', '2103', '無取件門市資料'),
(217, 'CVS', 'HILIFE', '2104', '門市臨時關轉店'),
(218, 'CVS', 'HILIFE', '3001', '轉運中(即集貨)'),
(219, 'CVS', 'HILIFE', '3002', '不在家'),
(220, 'CVS', 'HILIFE', '3003', '配完'),
(221, 'CVS', 'HILIFE', '3004', '送錯BASE (送錯營業所)'),
(222, 'CVS', 'HILIFE', '3005', '送錯CENTER(送錯轉運中心)'),
(223, 'CVS', 'HILIFE', '3006', '配送中'),
(224, 'CVS', 'HILIFE', '3007', '公司行號休息'),
(225, 'CVS', 'HILIFE', '3008', '地址錯誤，聯繫收件人'),
(226, 'CVS', 'HILIFE', '3009', '搬家'),
(227, 'CVS', 'HILIFE', '3010', '轉寄(如原本寄到A，改寄B)'),
(228, 'CVS', 'HILIFE', '3011', '暫置營業所(收件人要求至營業所取貨)'),
(229, 'CVS', 'HILIFE', '3012', '到所(收件人要求到站所取件)'),
(230, 'CVS', 'HILIFE', '3013', '當配下車(當日配送A至B營業所，已抵達B營業所)'),
(231, 'CVS', 'HILIFE', '3014', '當配上車(當日配送從A至B營業所，已抵達A營業所)'),
(232, 'CVS', 'HILIFE', '3015', '空運配送中'),
(233, 'CVS', 'HILIFE', '3016', '配完狀態刪除'),
(234, 'CVS', 'HILIFE', '3017', '退回狀態刪除(代收退貨刪除)'),
(235, 'CVS', 'HILIFE', '3018', '到店尚未取貨，簡訊通知取件'),
(236, 'CVS', 'HILIFE', '3019', '退件到店尚未取貨，簡訊通知取件'),
(237, 'CVS', 'HILIFE', '3020', '貨件未取退回物流中心'),
(238, 'CVS', 'HILIFE', '3021', '退貨商品未取退回物流中心'),
(239, 'CVS', 'HILIFE', '3022', '買家已到店取貨'),
(240, 'CVS', 'HILIFE', '3023', '賣家已取買家未取貨'),
(241, 'CVS', 'HILIFE', '3024', '貨件已至物流中心'),
(242, 'CVS', 'HILIFE', '3025', '退貨已退回物流中心'),
(243, 'CVS', 'HILIFE', '3029', '商品已轉換店（商品送達指定更換之取件店舖）'),
(244, 'CVS', 'HILIFE', '3031', '退貨商品已轉換店（退貨商品送達指定更換之取件店舖）'),
(245, 'CVS', 'HILIFE', '3032', '賣家已到門市寄件'),
(246, 'CVS', 'HILIFE', '4001', '退貨商品已至門市交寄'),
(247, 'CVS', 'HILIFE', '4002', '退貨商品已至物流中心'),
(248, 'CVS', 'HILIFE', '5001', '損壞，站所將協助退貨'),
(249, 'CVS', 'HILIFE', '5002', '遺失'),
(250, 'CVS', 'HILIFE', '5003', 'BASE列管(寄件人和收件人聯絡不到)'),
(251, 'CVS', 'HILIFE', '5004', '一般單退回'),
(252, 'CVS', 'HILIFE', '5005', '代收退貨'),
(253, 'CVS', 'HILIFE', '5006', '代收毀損'),
(254, 'CVS', 'HILIFE', '5007', '代收遺失'),
(255, 'CVS', 'HILIFE', '5008', '退貨配完'),
(256, 'CVS', 'HILIFE', '5009', '進貨門市發生緊急閉店，提早退貨至物流中心'),
(257, 'CVS', 'HILIFE', '7001', '超大(通常發生於司機取件，不取件)'),
(258, 'CVS', 'HILIFE', '7002', '超重(通常發生於司機取件，不取件)'),
(259, 'CVS', 'HILIFE', '7003', '地址錯誤，聯繫收件人'),
(260, 'CVS', 'HILIFE', '7004', '航班延誤'),
(261, 'CVS', 'HILIFE', '7005', '託運單刪除'),
(262, 'CVS', 'HILIFE', '7006', '小物流遺失'),
(263, 'CVS', 'HILIFE', '7007', '門市遺失'),
(264, 'CVS', 'HILIFE', '7008', '小物流破損，退回物流中心'),
(265, 'CVS', 'HILIFE', '7009', '商品包裝不良（物流中心反應）'),
(266, 'CVS', 'HILIFE', '7010', '商品包裝不良（門市反應）'),
(267, 'CVS', 'HILIFE', '7011', '取件門市閉店，轉退回原寄件店'),
(268, 'CVS', 'HILIFE', '7012', '條碼錯誤，物流中心客服處理'),
(269, 'CVS', 'HILIFE', '7013', '訂單超過驗收期限（商家未出貨）'),
(270, 'CVS', 'HILIFE', '7014', '商家未到貨（若訂單成立隔日未到貨即會發送，直到訂單失效刪除）'),
(271, 'CVS', 'HILIFE', '9001', '退貨已取'),
(272, 'CVS', 'HILIFE', '9002', '退貨已取'),
(273, 'CVS', 'HILIFE', '9999', '訂單取消'),
(274, 'HOME', 'ECAN', '311', '上傳退貨電子訂單處理中'),
(275, 'HOME', 'ECAN', '300', '訂單處理中(已收到訂單資料)'),
(276, 'HOME', 'ECAN', '310', '上傳電子訂單檔處理中'),
(277, 'HOME', 'ECAN', '325', '退貨訂單處理中(已收到訂單資料)'),
(278, 'HOME', 'ECAN', '3003', '配完'),
(279, 'HOME', 'ECAN', '3110', '轉配郵局'),
(280, 'HOME', 'ECAN', '3111', '配送外包'),
(281, 'HOME', 'ECAN', '3006', '配送中'),
(282, 'HOME', 'ECAN', '3112', '再配'),
(283, 'HOME', 'ECAN', '3113', '異常'),
(284, 'HOME', 'ECAN', '3114', '再取'),
(285, 'HOME', 'TCAT', '311', '上傳退貨電子訂單處理中'),
(286, 'HOME', 'TCAT', '300', '訂單處理中(已收到訂單資料)'),
(287, 'HOME', 'TCAT', '310', '上傳電子訂單檔處理中'),
(288, 'HOME', 'TCAT', '325', '退貨訂單處理中(已收到訂單資料)'),
(289, 'HOME', 'TCAT', '3001', '轉運中(即集貨)'),
(290, 'HOME', 'TCAT', '3002', '不在家'),
(291, 'HOME', 'TCAT', '3004', '送錯BASE (送錯營業所)'),
(292, 'HOME', 'TCAT', '3005', '送錯CENTER(送錯轉運中心)'),
(293, 'HOME', 'TCAT', '3006', '配送中'),
(294, 'HOME', 'TCAT', '3007', '公司行號休息'),
(295, 'HOME', 'TCAT', '3008', '地址錯誤，聯繫收件人'),
(296, 'HOME', 'TCAT', '3009', '搬家'),
(297, 'HOME', 'TCAT', '3010', '轉寄(如原本寄到A，改寄B)'),
(298, 'HOME', 'TCAT', '3011', '暫置營業所(收件人要求至營業所取貨)'),
(299, 'HOME', 'TCAT', '7002', '超重(通常發生於司機取件，不取件)'),
(300, 'HOME', 'TCAT', '7003', '地址錯誤，聯繫收件人'),
(301, 'HOME', 'TCAT', '5005', '代收退貨'),
(302, 'HOME', 'TCAT', '3017', '退回狀態刪除(代收退貨刪除)'),
(303, 'HOME', 'TCAT', '3003', '配完'),
(304, 'HOME', 'TCAT', '5001', '損壞，站所將協助退貨'),
(305, 'HOME', 'TCAT', '5002', '遺失'),
(306, 'HOME', 'TCAT', '5003', 'BASE列管(寄件人和收件人聯絡不到)'),
(307, 'HOME', 'TCAT', '5004', '一般單退回'),
(308, 'HOME', 'TCAT', '3015', '空運配送中'),
(309, 'HOME', 'TCAT', '3016', '配完狀態刪除'),
(310, 'HOME', 'TCAT', '7005', '託運單刪除'),
(311, 'HOME', 'TCAT', '5006', '代收毀損'),
(312, 'HOME', 'TCAT', '5007', '代收遺失'),
(313, 'HOME', 'TCAT', '5008', '退貨配完'),
(314, 'HOME', 'TCAT', '3012', '到所(收件人要求到站所取件)'),
(315, 'HOME', 'TCAT', '3013', '當配下車(當日配送A至B營業所，已抵達B營業所)'),
(316, 'HOME', 'TCAT', '3014', '當配上車(當日配送從A至B營業所，已抵達A營業所)'),
(317, 'HOME', 'TCAT', '7001', '超大(通常發生於司機取件，不取件)'),
(318, 'HOME', 'TCAT', '7004', '航班延誤'),
(319, 'HOME', 'TCAT', '7024', '另約時間'),
(320, 'HOME', 'TCAT', '7025', '電聯不上'),
(321, 'HOME', 'TCAT', '7026', '資料有誤'),
(322, 'HOME', 'TCAT', '7027', '無件可退'),
(323, 'HOME', 'TCAT', '7028', '超大超重'),
(324, 'HOME', 'TCAT', '7029', '已回收'),
(325, 'HOME', 'TCAT', '7030', '別家收走'),
(326, 'HOME', 'TCAT', '7031', '商品未到');

-- --------------------------------------------------------

--
-- 資料表結構 `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT '訂單編號',
  `logisticType` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '物流廠商',
  `product` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '商品名稱',
  `price` int(50) DEFAULT NULL COMMENT '貨物價值',
  `shipment` int(10) DEFAULT NULL COMMENT '1 純配送 2 貨到付款',
  `receiveStore` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收件門市',
  `receiveName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收件人',
  `receivePhone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT '收件人電話',
  `receiveEmail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `other` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ck_order` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `createDate` datetime NOT NULL,
  `updateDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `logisticType`, `product`, `price`, `shipment`, `receiveStore`, `receiveName`, `receivePhone`, `receiveEmail`, `other`, `ck_order`, `createDate`, `updateDate`) VALUES
(11, '900573780991', '1', '測試匯入', 1500, 1, '149697', '周子瑜', '0928887788', 'test@gmail.com', 'test', 'Y', '2019-04-16 08:18:15', '2019-04-17 08:36:21'),
(12, '900573780980', '2', '測試匯入2', 1500, 1, '3750', '周子瑜', '0928887788', '0928887788', 'test2', 'Y', '2019-04-16 08:18:15', '2019-04-17 08:36:15'),
(13, '900573780976', '3', '測試匯入3', 15000, 1, '896218', '周子瑜', '0928887788', '0928887788', 'test2', 'Y', '2019-04-16 08:18:15', '2019-04-17 08:36:07'),
(14, '0928788780', '2', 'test', 9999, 1, '014482', '王伍美', '0928888888', NULL, NULL, 'Y', '2019-04-17 06:12:14', '2019-04-18 07:08:20'),
(15, '2019041801', '1', '範例商品1', 1000, 1, '174012', '李一', '0928123457', 'test@gmail.com', '1', 'Y', '2019-04-19 02:38:52', '2019-04-19 02:38:52'),
(16, '2019041802', '1', '範例商品2', 1000, 2, '174013', '李一', '0928123458', 'test@gmail.com', '2', 'Y', '2019-04-19 02:42:31', '2019-04-19 02:42:31'),
(17, '2019041803', '1', '範例商品3', 1001, 3, '174014', '李一', '0928123459', 'test@gmail.com', '3', 'Y', '2019-04-19 02:42:31', '2019-04-19 02:42:31');

-- --------------------------------------------------------

--
-- 資料表結構 `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `parent` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `mid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `c_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `c_tel` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ecs_code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `search_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `incode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `outcode` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `in_time` datetime NOT NULL,
  `out_time` datetime DEFAULT NULL,
  `logistic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CVSPaymentNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CVSValidationNo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ck_order` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RtnCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RtnMsg` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 資料表的匯出資料 `product`
--

INSERT INTO `product` (`id`, `parent`, `mid`, `c_name`, `c_tel`, `status`, `ecs_code`, `search_code`, `incode`, `outcode`, `in_time`, `out_time`, `logistic`, `CVSPaymentNo`, `CVSValidationNo`, `ck_order`, `RtnCode`, `RtnMsg`, `createDate`, `updateDate`) VALUES
(48, '123456', 'm00004', 'eason', '0928888888', '寄貨', '20190417113628282', NULL, '900573780980', '175398', '2019-04-17 03:36:26', '2019-04-17 03:36:26', 'UNIMARTC2C', NULL, NULL, 'Y', '300', '訂單處理中(已收到訂單資料)', '2019-04-17 03:36:26', '2019-04-17 03:36:26'),
(75, 'testsetsetset', 'm00003', 'eason', '0928888888', '寄貨', '20190418150759470', '175987', '900573780991', NULL, '2019-04-17 09:31:49', '2019-04-18 00:00:00', 'UNIMART', NULL, NULL, 'Y', '300', '訂單處理中(已收到訂單資料)', '2019-04-17 09:31:49', '2019-04-17 09:31:49'),
(80, 'test123', 'm00001', 'eason', '0928888888', '寄貨', '20190422143835329', '176392', '900573780976', NULL, '2019-04-22 06:38:35', '2019-04-22 00:00:00', 'FAMI', NULL, NULL, 'Y', '300', '訂單處理中(已收到訂單資料)', '2019-04-22 06:38:35', '2019-04-22 06:38:35');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `col`
--
ALTER TABLE `col`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `lgstatus`
--
ALTER TABLE `lgstatus`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- 使用資料表 AUTO_INCREMENT `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- 使用資料表 AUTO_INCREMENT `col`
--
ALTER TABLE `col`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- 使用資料表 AUTO_INCREMENT `lgstatus`
--
ALTER TABLE `lgstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=327;

--
-- 使用資料表 AUTO_INCREMENT `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- 使用資料表 AUTO_INCREMENT `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
