<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('visit')->group(function () {
//登入
Route::get('/test','IndexController@test');
Route::any('/login','IndexController@login');
Route::any('/logout','IndexController@logout');

    //驗證//
    Route::middleware('redec')->group(function () {

        Route::get('/', function () {
            return redirect('/fixedManage');
        });

        /*維修單管理*/
        Route::get('/fixedManage','FixedController@getList');
        Route::any('/getFixed','FixedController@getFixed');
        Route::any('/getFixedData','FixedController@getFixedData');
        Route::any('/fixedEdit','FixedController@fixedEdit');
        Route::any('/FixedAdd','FixedController@FixedAdd');
        Route::any('/FixedDelete','FixedController@FixedDelete');
        Route::any('/viewFixed','FixedController@viewFixed');

         //材料管理
         Route::get('/PartManage','PartController@Partlist');
         Route::any('/getPart','PartController@getPart');
         Route::any('/getPartData','PartController@getPartData');
         Route::any('/partEdit','PartController@partEdit');
         Route::any('/PartAdd','PartController@PartAdd');
         Route::any('/partDelete','PartController@partDelete');
         //機種管理
         Route::get('/model1','ModelController@getModel1');
         Route::any('/getModel1List','ModelController@getModel1List');
         Route::any('/model1Add','ModelController@model1Add');
         Route::any('/model1Edit','ModelController@model1Edit');
         Route::any('/getmodel1Data','ModelController@getmodel1Data');
         Route::any('/model1Delete','ModelController@model1Delete');
         //機型管理
         Route::get('/model2','ModelController@getModel2');
         Route::any('/getModel2List','ModelController@getModel2List');
         Route::any('/model2Add','ModelController@model2Add');
         Route::any('/model2Edit','ModelController@model2Edit');
         Route::any('/getmodel2Data','ModelController@getmodel2Data');
         Route::any('/model2Delete','ModelController@model2Delete');
         Route::any('/getModelItem','ModelController@getModelItem');


        //客戶管理
        Route::get('/customerManagement','MemberController@memberlist');
        Route::any('/getmember','MemberController@getmember');
        Route::any('/getdata','MemberController@getdata');
        Route::any('/memberEdit','MemberController@memberEdit');
        Route::any('/memberAdd','MemberController@memberAdd');
        Route::any('/memberDelete','MemberController@memberDelete');


        //-帳號設置
        Route::get('/generalInformation','PermissionController@settingName');
        Route::any('/setting/update','PermissionController@settingUpdate');
        Route::any('/updPassword','PermissionController@updPassword');

        Route::get('/changePassword', 'PermissionController@settingPassword');

    });
});
